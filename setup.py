from setuptools import setup, find_packages

from planificados_models import __version__

extra_dependencies = {
    'v1': [
        'pydantic==1.8.1',
    ],
}
extra_dependencies['all'] = list(set(sum(extra_dependencies.values(), [])))

setup(
    name='planificados-models',
    version=__version__,
    description='This package contains planificados_models for interacting with the planificados project',
    author='Ilichev Andrey',
    author_email='ilichev.andrey.y@gmail.com',
    url='https://gitlab.com/planificados/planificados-models.git',
    packages=find_packages(exclude=('tests',)),
    extras_require=extra_dependencies,
    python_requires='>=3.9',
)
