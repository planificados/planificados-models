from typing import Generic, List, Optional, TypeVar
from uuid import UUID

from pydantic import BaseModel, EmailStr
from pydantic.generics import GenericModel

from planificados_models.v1.enums import UserType

DataT = TypeVar('DataT')


class BasePlanificadosModel(BaseModel):
    class Config:
        orm_mode = True


class MessageResponse(BasePlanificadosModel):
    message: str


class Items(GenericModel, Generic[DataT]):
    items: List[DataT]


class ItemsResponse(Items):
    total: int


class MessengerIds(BasePlanificadosModel):
    telegram_id: Optional[str]
    viber_id: Optional[str]


class User(MessengerIds):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    email: Optional[EmailStr] = None
    phone_number: Optional[str] = None
    telegram_id: Optional[str] = None
    telegram_name: Optional[str] = None
    viber_id: Optional[str] = None
    viber_name: Optional[str] = None


class UserResponse(User):
    id: UUID
    is_admin: bool
    type: UserType


class Service(BasePlanificadosModel):
    name: str
    worker_id: UUID
    execution_time_minutes: int


class GetServiceResponse(Service):
    id: UUID


class CreateService(BasePlanificadosModel):
    name: str
    execution_time_minutes: int


class TimetableResponse(BasePlanificadosModel):
    pass


class AuthResponse(BasePlanificadosModel):
    access_token: str
    token_type: str


class GetToken(MessengerIds):
    pass
